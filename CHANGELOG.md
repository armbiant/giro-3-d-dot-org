# Changelog

## v0.22.0 (2023-03-16)

### Feat

- Expose API to track progress of data processing (#237). See [the example](https://giro3d.org/examples/tracking_progress.html).
- **LayeredMaterial**: support transparent backgrounds (#245). See [the example](https://giro3d.org/examples/transparent_map_bg.html).
- **examples**: add [custom controls](https://giro3d.org/examples/camera_controls.html) example (#235).

### Fix

- **Map**: combine all colormaps into an atlas (#244). This helps reduce the number of texture units consumed by a map tile.
- **OLTileProvider**: filter out requests that returned null (#242)
- **Layer**: handle null textures (#242)
- **TileFS**: avoid warning when `gl_FragColor` is not set before discarding the fragment (#241)

## v0.21.0 (2023-03-06)

This release contains many bugfixes and performance improvements, as well as two features: the `DrawTools` and the `HttpConfiguration` modules.

### BREAKING CHANGE

- `Fetcher` has been moved to `utils/`.
- `Cache` has been moved to `core/`.
- `Cache` is now an instantiable class. To use the global singleton cache, use `GlobalCache`:

    ```js
    import { GlobalCache } from '../core/Cache.js';

    const foo = GlobalCache.get('foo');

    GlobalCache.purge();
    GlobalCache.clear();
    ```

### Feat

- **Drawtools**: add the `DrawTools` class to draw geometries on various objects. (#5). [See it in action](https://giro3d.org/examples/drawtool.html).
- add the `HttpConfiguration` module (#86). This module stores configuration (such as headers) based on URL paths. [See the documentation](https://giro3d.org/apidoc/module-utils_HttpConfiguration.html) for a detailed explanation.
- **Inspector**: display min/max of elevation layers
- **Inspector**: add Instance inspector

### Fix

- **Map**: properly dispose pooled geometries (#230)
- **TextureGenerator**: fix forever pending `create8bitImage()`  (#232)
- **Inspector**: add counters for pending/cancelled/failed/running/complete commands
- **Scheduler**: don't log cancelled commands
- **MemoryTracker**: fix non-weakrefs

### Refactor

- **Fetcher**: improve message in `checkResponse()`
- **Fetcher**: move to `utils/` folder

### Perf

- **Cache**: prevent unbounded growth (#225). This uses the `lru-cache` package to ensure that the cache capacity is controlled.
- **OLTileProvider**: handle command cancellation (#238)
- **COGProvider**: handle command cancellation (#234)
- **Map**: don't keep data copy of elevation textures (#215). This considerably reduces the memory usage of scenes that contain elevation data.
- **Picking**: don't sample the elevation texture (#231)
- **3DEngine**: option to toggle shader validation (#229). Shader validation is a costly operation that should be avoided in production.
- **Map**: avoid allocating too many `TileGeometry` objects (#230)

## v0.20.1 (2023-02-17)

Hotfix for the #228 issue.

### Fix

- **c3DEngine**: clear canvas to avoid flickering
- **WebGLComposer**: restore clear alpha (#228)

## v0.20.0 (2023-02-16)

Lots of bugfixes and performance improvements around texture handling.

### BREAKING CHANGE

- The method `instance.getLayers()` is removed, please call `getLayers()`
  on relevant entities instead.
- `instance.getOwner(layer)` is removed, just use `layer.owner` instead.

### Feat

- **formats**: enable shader Y-flipping of DataTextures (#202)

### Fix

- **COGProvider**: stop compressing data to 8-bit (#216)
- **PointsMaterial**: add a missing needsUpdate = true (#200)
- **PointsMaterial**: fix memory leak of color textures
- **Helpers**: fix remove3DTileBoundingVolume() (#150)
- **PointsMaterial**: fix black tiles in overlay mode (#219)
- **WebGLComposer**: don't `clear()` the renderer
- **Map**: throw error if the extent is invalid (#218)
- **OLTileProvider**: use tile ratio for zoom level (#144)
- **Map**: set a limit on the aspect ratio (#144)
- **OLTileProvider**: handle 204 No Content responses (#206)
- **COGProvider**: fix memory leak of cached textures

### Refactor

- **Layer**: move `frozen` property up to Layer
- move and rename `Layer.defineLayerProperty` to `EventUtils.definePropertyWithChangeEvent`
- **Instance**: remove `instance.getLayers()` and `instance.getOwner()`
- remove or fix some dependencies links between our modules
- remove `threejslayer` support for Entities

### Perf

- **Tiles3D**: use lower resolution texture overlays
- **COGProvider**: flip the textures using the `WebGLComposer` (#202)

## v0.19.1 (2023-02-06)

### Feat

- **ElevationLayer**: expose `minmax` in constructor (#190)
- **Entity**: add `dispose()` method. This method was previously undocumented.

### Fix

- **AxisGrid**: honors visibility in `refresh()` (#214)
- **TileFS**: honor layer alpha with color maps (#212)

## v0.19.0 (2023-02-02)

### Feat

- **AxisGrid**: add the AxisGrid entity (#165). See the [example](https://giro3d.org/examples/axisgrid.html).
- **Extent**: add `topLeft()`, `topRight()`, `bottomLeft()`, `bottomRight()`
- **Helpers**: add a function to create an `ArrowHelper`
- **geojsonparser**: make `crsOut` parameter default to `crsIn`

### Fix

- **LayeredMaterial**: fix memory leak where color textures were not properly disposed.
- **Instance**: properly propagate preprocessing error

### Refactor

- **Entity3d**: make the `visible` property overiddable
- **Entity3D**: make the `opacity` property overridable

## v0.18.0 (2023-01-20)

### BREAKING CHANGE

- All source folders are now in lowercase (#130). For example:
    ```js
    import Instance from '@giro3d/giro3d/Core/Instance.js';
    import Extent from '@giro3d/giro3d/Core/Geographic/Extent.js';
    ```
    Becomes
    ```js
    import Instance from '@giro3d/giro3d/core/Instance.js';
    import Extent from '@giro3d/giro3d/core/geographic/Extent.js';
    ```

- Upgrade THREE to v0.148. If you are using THREE in your application along Giro3D, you will need
  the [THREE migration guide](https://github.com/mrdoob/three.js/wiki/Migration-Guide) (#153).

- We no longer transpile Giro3D to support older browsers. Instead, we support the browsers list in `package.json` directly from the source code (#89)

### Feat

- **Inspector**: make width an option and increase default width to 450px
- **Inspector**: add inspector for the sources
- **Extent**: add `withMargin()` and `withRelativeMargin()` methods to add margins to extents.

### Fix

- **LayeredMaterial**: support changing the type of the atlas texture (#192)
- **Interpretation.glsl**: honor texture alpha in scaling mode
- **TileFS.glsl**: don't process the color layer if its opacity is zero
- **Extent**: fix weird construct in `center()` that trigger errors
- **Instance**: only use `ResizeObserver` if it is available
- fix import to `THREE.MathUtils`
- **CachePanel**: fix `dump()` to output an array instead of an iterator
- **Map**: create texture slightly biggers than tile to artifacts in atlas and hillshading (#27, #156)

### Refactor

- set all folders to lowercase

## v0.17.0 (2023-01-09)

A small bugfix release.

### Feat

- **examples**: add mouse coordinates

### Fix

- **COGProvider**: guarantee that the generated texture is at least one pixel wide
- **TileFS.glsl**: fix Mac-specific implementation issues around atan()
- **DEMUtils**: fix reading elevation texture with COGs
- **TileVS**: fix corner stitching when neighbours are bigger than the current tile

## v0.16.0 (2023-01-05)

This release generalizes color maps for all layers (and not only the elevation layer), support for
the BIL elevation format, and lots of bugfixes.

### BREAKING CHANGE

- ColorMaps are now a property of layers, instead of map. See the [example](https://giro3d.org/examples/colormaps.html) for more information :
    ```js
    import * as THREE from 'three';
    import ColorMap from '@giro3d/giro3d/Core/layer/ColorMap.js';
    import ColorMapMode from '@giro3d/giro3d/Core/layer/ColorMapMode.js';

    const min = 100;
    const max = 1500;
    const colors = [new THREE.Color('red'), new THREE.Color('blue'), new THREE.Color('green')];

    const colorLayer = new ColorLayer('color', {
        colorMap: new ColorMap(colors, min, max, ColorMapMode.Elevation)
    });
    ```
- The property `noTextureColor` is removed. To set the background color
  of the map, use the `backgroundColor` constructor option :
    ```js
    const map = new Map('myMap', { backgroundColor: 'red' });
    ```
- The property `elevationFormat` of the `ElevationLayer` is replaced by the `interpretation` option
  in the `Layer` class:
    ```js
    import Interpretation from '@giro3d/giro3d/Core/layer/Interpretation.js';

    const layer = new ElevationLayer('myLayer', {
            interpretation: Interpretation.ScaleToMinMax(elevationMin, elevationMax)
    });
    ```
  This property can be used for any layer that needs special processing, such as [Mapbox Terrain RGB](https://docs.mapbox.com/data/tilesets/reference/mapbox-terrain-rgb-v1/) format.

### Feat

- **ColorMap**: Introduce class `ColorMap` and make it available for all layers.
- **Inspector**: add section for color maps
- **Map**: remove ColorMap from Map and put it in Layer
- **MapInspector**: add toggle for hillshading
- **Inspector**: show the map and layer extents as 3D bounding boxes
- **Map**: add the `getMinMax()` method
- **Extent**: add the `toBox3()` method to convert to a `THREE.Box3`
- **Map**: the `segments` property is now dynamic
- **example**: add example for WMTS layers and MapBox elevation layer
- Add support for BIL elevation format

### Fix

- **FirstPersonControls.js**: add a target in `eventToCanvasCoords()`
- **Picking**: fix incorrect result of `pickTilesAt()`
- **CustomTiledImageProvider**: draw all images that intersect with the query extent
- **LayeredMaterial**: only dispose color textures that we own
- **Inspector**: fix missing update when changing an object's properties
- **LayeredMaterial**: fix broken `update()` that would ignore shader recompilation
- **TextureGenerator**: set `needsUpdate = true` to texture generated by `decodeBlob()`
- **Inspector**: fix scrolling artifact on chrome/edge
- **examples**: fix inspector layout

### Refactor

- **Map**: add the `setRenderState()` method
- decode elevation formats in the provider
- **ElevationLayer**: simplify min/max calculation

## v0.15.0 (2022-12-15)

### BREAKING CHANGE

- The function `Extent.offsetScale()` is removed. Please use `Extent.offsetToParent()` instead.
- Map: the `segments` option passed in the constructor options MUST be a power of two.
- Map: the property `noTextureOpacity` is removed. Please use `opacity` instead :

  ```js
  map.opacity = 0.5;
  ```

### Feat

- **MapInspector**: add drop-down list to change the render state of the tiles
- **MapInspector**: enable opacity slider

### Fix

- **Map**: fix opacity issues
- **Map**: support stitching including the 4 diagonal neighbours
- **WebGLComposer**: disable mip-maps for generated textures
- **Coordinates**: fix Y-axis inversion in `offsetToExtent()`

### Refactor

- **Extent**: delete unused and buggy `offsetScale()` function

## v0.14.0 (2022-12-08)

### BREAKING CHANGE

- `OGCWebServiceHelper` has been removed. If you need to get textures directly, use `Fetcher` to
  download images and `TextureGenerator` to process them into textures.
- `Instance.getObjects()` returned array now contains the Object3D directly added with
`Instance.add()`. This makes the API more consistent, but could break applications not expecting this.
- The `TMSProvider` class has been removed. Use the `OLTileProvider` in combination with OpenLayers
  tiled sources to process tiled layers. See this [example](https://giro3d.org/examples/orthographic.html)
  for more information.

### Feat

- **OLTileProvider**: support tiles in the `image/tiff` format. See this [example](https://giro3d.org/examples/tifftiles.html)
  on how to configure it
- **ElevationLayer**: add the `noDataValue` option. Useful when the no-data value cannot be inferred from the downloaded tiles.
- **Inspector**: add a Cache panel to track cached entries and manipulate the cache.

### Fix

- **Composer**: always use WebGL implementation if `createDataCopy` is true
- **ElevationLayer**: handle `DATA_UNAVAILABLE` in preprocessing step
- **Instance**: make `getObjects()` returns THREE.js objects
- **Inspector**: fix visibility toggle for Tiles3D
- **LayeredMaterial**: only dispose the elevation texture if it is not inherited

### Refactor

- **CustomTileProvider**: use `Fetcher` and `TextureGenerator` instead of `OGCWebServiceHelper`
- **TMSProvider**: remove obsolete `TMSProvider` to use ol sources

## v0.13.0 (2022-12-01)

This version adds support for HTML labels, by leveraging THREE's `CSS2DRenderer`, as well as various
bugfixes and performance improvements.

### BREAKING CHANGE

`Map` is now a default export. Update your imports from :

```js
import { Map } from '@giro3d/giro3d/entities/Map';
```
to
```js
import Map from '@giro3d/giro3d/entities/Map';
```

### Feat

- **Renderer**: add CSS2DRenderer support (see `htmllabels` example)
- **Inspector**: display the versions of Giro3D, OpenLayers and THREE
- **Cache**: call an optional callback when an entry is deleted
- **ColorLayer**: add option to display the image borders

### Fix

- **Instance.js**: make sure we use valid Entity reference when adding object
- **OLTileProvider**: fix incorrect tile placement due to rounding errors
- **OLTileProvider**: return DATA_UNAVAILABLE when below the min zoom level

### Refactor

- merge `TileProvider` into `Map`

### Perf

- **Map**: improve performance of neighbour fetching by using a tile index
- **OLTileProvider**: temporarily cache source tiles

## v0.12.0 (2022-11-24)

Notable features :

- support for high dynamic range textures in maps. This enables native support
for 32-bit floating point elevation data for example, without compressing the pixels to 8-bit.
- Maps can now be displayed in double sided with the option `doubleSided` in the constructor. The
backside is displayed in a desaturated, darker tone.
- The Giro3D context can be unloaded with the `Instance.dispose()` method.

### BREAKING CHANGE

- In `Instance`, `normalizedToCanvasCoords()`, `canvasToNormalizedCoords()`,
`eventToNormalizedCoords()` and `eventToCanvasCoords()` now require a `target` vector passed as
parameter to reduce memory allocations.

### Feat

- Add support for high dynamic range textues
- `OLTileProvider` : add support for extended tile formats (each new format must be implemented)
- **Inspector**: add position of camera target, if any
- **TileFS**: display backside fragments in a desaturated, darker tone
- **Map**: add the doubleSided option
- **examples**: add instance disposal example
- **Instance**: add the dispose() method to unload the Giro3D context

### Fix

- **Layer**: don't resolve whenReady before processing is done
- **CanvasComposer**: support textures in addition to images
- **Composer**: improve robustness of implementation selector
- **WebGLComposer**: fix memory leaks
- **TileFS**: fix faulty alpha blending
- make sure Helpers and OBBHelper correctly clean memory
- make sure panels and inspectors don't leak memory

### Refactor

- **OLTileProvider**: delegate the tile image decoding to TextureGenerator
- **TextureGenerator**: the default export is now an object
- **providers**: remove unused tileTextureCount()
- **TileMesh**: implement dispose()
- **MemoryTracker**: group tracked objects by type in getTrackedObjects()
- **TileFS**: display outlines with different colors for each border
- **Instance**: use target vectors for coordinate related API

## v0.11.0 (2022-11-17)

### Feat

- **Map**: adjust the number of subdivisions to fit the tile width/height ratio

### Fix

- **TileMesh**: assign a default bbox thickness of 1 meter
- **TileVS**: use 2 neighbours instead of 1 for corner vertices while stitching
- **examples**: support dynamic GLSL recompilation in webpack config

## v0.10.0 (2022-11-10)

### Feat

- **TextureGenerator**: replace nodata values by valid values to remove interpolation effect
- **TileGeometry**: remove nodata triangulation
- **Stiching**: use geometry dimension instead of segments to allow rectangular stiching

### Fix

- **Extent**: proper center to target
- **Inspector**: fix missing layer opacity slider when opacity is zero

### Refactor

- **Extent**: remove quadTreeSplit, replace by split(2, 2)

## v0.9.0 (2022-11-03)

### Fix

- **OLTileProvider**: skip source tiles outside the layer's extent
- **OLVector*Provider**: render the image in own canvas instead of tile atlas
- **LayerInspector**: truncate the layer names when they are too long

## v0.8.0 (2022-10-27)

This release contain mainly bugfixes related to maps and elevation layers, as well as features
regarding no-data handling in elevation layers.

### Feat

- **TextureGenerator**: consider NaN values as no-data when relevant
- **Map**: add an option to discard no-data elevation pixels in the fragment shader

### Fix

- **Map**: tile outlines are now available even in non-DEBUG builds
- **LayeredMaterial**: fix memory leak of colormap textures
- **LayeredMaterial**: fix issues where semi-transparent tile images would be drawn on top of each other
- **LayeredMaterial**: use the exact size of the elevation texture for hillshading
- **ElevationLayer**: fix missing redraw after updating the elevation texture of a tile

### Perf

- **Inspector**: update panels only if they are open
- **TileGeometry**: recycle object for dimensions computation

## v0.7.0 (2022-10-20)

This release contains a lot of features and bugfixes related to map and terrain rendering, most notably colormapping and hillshading. No breaking changes.

### Feat

- **LayeredMaterial**: hillshading and colormaps can now be toggled
- handle raw elevation values in shaders
- **Picking**: add filter options to filter results
- **Picking**: add support of filter option for all picking methods
- **Picking**: add limit options to limit the number of items to pick
- **Colormapping**: colorize elevation, slope and aspect by an color array LUT
- **Hillshading**: parametrize light directions for hillshade calculation

### Fix

- **LayeredMaterial**: fix incorrect manipulation of elevation defines
- **OLTileProvider**: handle missing tiles in the source
- **DEMUtils**: handle non-byte elevation textures
- **COGProvider**: handle 32-bit float data
- **DEMUtils**: fix reading value from textures image data
- **DEMUtils**: normalize pixel value coming from image.data and fix flipY=false
- **DEMUtils**: correct elevation from textures image data
- **Picking**: fix where filter to ensure object is supported
- **Picking**: fix mouse position when picking on multiple sources
- **LayeredMaterial.js**: remove delayed repaint of the atlas
- **examples**: fix elevation values

### Refactor

- **Picking**: started cleaning-up pickObjectsAt redefinitions
- **Picking**: passing radius&filter as options
- **lightDirection**: expose lightDirection to the Map entity
- **Hillshading**: change where the hillshade options are set and applied (better API)

### Perf

- **Picking**: avoid creating unnecessary arrays when picking

## v0.6.0 (2022-10-13)

This release contains mainly bugfixes around HTML layout and map display. No breaking changes.

### Feat

- **Instance**: warn if the supplied host div is not an Element or has children
- **Inspector**: add button to dump map tiles in the console
- **ElevationLayer**: inherit from root texture when none available
- **Inspector**: expose the .visible property of layers

### Fix

- **styling**: make sure canvas resizing works well for any layout
- **Layer**: ensure that all preprocessings are finished before setting ready = true
- **ElevationLayer**: find ancestor with a reusable texture instead of only the direct parent
- **ColorLayer,ElevationLayer**: don't update anything until the layer is ready
- **Map**: don't preprocess the layer twice
- **OLTileProvider**: don't override layer.extent
- **Map**: enforce layer ordering in an async context
- **TileFS/HillShading**: correct UV for hillshading
- **Cache**: fix clear() that was not a valid function

### Refactor

- **styling**: always create viewport in instance
- **styling**: simplify size computation
- **Instance**: better resize observer
- **ColorLayerOrdering**: remove unused class
- **CustomTiledImage**: fix dead/deprecated code

## v0.5.0 (2022-10-10)

This releases contain many bugfixes and improvements related to maps and layers, as well as the Inspector class to inspect and help debug the Giro3D instance.

### BREAKING CHANGE

- PlanarTileBuilder, PanoramaTileBuilder and PanoramaView are removed. All tiles are
now considered planar (including terrain deformation). Note: PanoramaView may be restored in a future release.
- PotreePointCloud: computeScreenSpaceError() uses point radius instead of diameter. Some adjustment may be needed in existing code to take into account the change in the resulting SSE.

### Feat

- **COGProvider/ElevationLayer**: add elevation cog processing to demonstrate nodata triangulation
- **TileGeometry**: update tile geometry with elevation data, triangulate according to nodata
- **Map**: let the map select the best tile size
- **Map**: subdivide extent into level 0 tiles to minimize distortion
- **Extent**: add the split() method
- **Instance.js**: support adding and removing threejs objects
- **Map**: expose the backgroundColor option
- **Map**: expose the segments property in the options
- **Map**: supports hillshading through options
- **GeographicCanvas**: add the getImageData() method
- **Inspector**: add custom inspector for PotreePointCloud
- add PotreePointCloud entity
- **Instance**: improve focusObject() to be more generic
- **Inspector**: add the Inspector
- **Helpers**: add Helpers class
- **Instance**: add events when an entity is added/removed
- **Map**: fire events layer-added and layer-removed

### Fix

- **Instance**: handle resizing the containing DOM element
- **TileGeometry.test**: adapt the tests to the latest changes
- **TileGeometry/ElevationLayer**: create a new geometry instead of updating buffers
- **hillshade**: fix camera orientation issue in example
- **Packer**: stop forcing the image sizes in power of two
- **examples**: fix broken URL after release of OL7
- **TileFS.glsl**: fix absence of hillshading when no color layer is present
- **TileFS.glsl**: fix reversed sampling of elevation texture
- enable wireframe and tile outlines in __DEBUG__
- **Map**: allow arbitrary EPSG:3857 extents
- **LayeredMaterial**: ensure the atlas is correctly initialized before drawing into it
- **ColorLayer**: fix undefined error if parent has no color texture
- **PotreePointCloud**: computeScreenSpaceError() uses point radius instead of diameter
- **Layer.js**: allow more openlayers sources
- **OLTileProvider**: support arbitrary number of tiles per Map tile
- **planartilebuilder**: center correctly planar map tiles when using geographic extents
- **Map**: rewrite removeLayer() to actually remove the layer

### Refactor

- **COGProvider/ColorLayer/ElevationLayer**: architecture changes for the review
- **providers**: remove unused fx property
- **Map**: remove unused property validityExtent
- **TileGeometry**: simplify, comment and accelerate TileGeometry creation
- **Prefab/**: remove Prefab/ (PlanarTileBuilder, PanoramaTileBuilder and PanoramaView)
- **OLTileProvider**: fix type signature of loadTiles()
- **Map**: remove disableSkirt property
- rename view in instance
- add names to THREE objects
- **entities/layers**: add 'type' property to help with Inspector
- **LayerUpdateStrategy**: create an object that contains all strategies
- **Layer**: rename clean() -> dispose()

### Perf

- **OLTileProvider**: further optimize the generation of tile textures
- **TileGeometry**: rewrote TileGeometry in the fashion of SmartGrid
- **COGProvider**: produce DataTextures instead of Textures
- **OLTileProvider**: produce DataTextures instead of Textures
- **LayeredMaterial**: support DataTextures

## v0.4.0 (2022-08-29)

### BREAKING CHANGE

- `Instance.removeObject` has been renamed, please use `Instance.remove` instead.
- `3dtiles` data source should now use `Tiles3D` instead of `GeometryLayer`. Example code:
```js
import Tiles3D from '../src/entities/Tiles3D.js';
import Tiles3DSource from '../src/sources/Tiles3DSource.js';

const pointcloud = new Tiles3D(
    'pointcloud',
    new Tiles3DSource('https://3d.oslandia.com/3dtiles/eglise_saint_blaise_arles/tileset.json'),
);
```
- The protocols 'rasterizer' and 'wfs' should be remplaced by a Vector source. The
protocol 'wms' should be remplaced by a WMS source.
- Map.addLayer requires a Layer, ColorLayer or a ElevationLayer, not an
object anymore. Please see `./examples/planar.js` for instance.
- Instance.getParentLayer() has been renamed to  Instance.getOwner()

### Feat

- **Instance**: add domElement accessor
- **entities**: add the Tiles3D entity
- **Instance**: add registerCRS() static method

### Fix

- **Instance**: reject when object is not of the correct type
- **DEMUtils.js**: fix offset calculation from parent texture when picking
- **DEMUtils.js**: fix a call to THREE API (a leftover from an upgrade of THREE)
- **DEMUtils.js**: fix the logic of getting the correct tile we pick from
- **LayeredMaterial.js**: stop using arbitrary long timeout for refreshing atlas
- correctly inherit textures from parent tiles
- **examples**: use useTHREEcontrols() everywhere
- **examples**: use MapControls instead of OrbitControls to be consistent
- **TileFS.glsl**: don't compile shader loop if TEX_UNITS is not defined
- **PointCloud**: fix colorizing a pointcloud via a layer
- **cog example**: fix the layer selection in the example
- **examples**: fix searchbox ignoring upper case text
- **StaticProvider**: fix usage of StaticProvider with new source
- **Map**: provide optional object3d in constructor
- **examples**: define the __DEBUG__ variable if mode = 'development'
- **examples**: fix path to giro3d.js bundle

### Refactor

- **DEMUtils**: remove some useless function parameters
- **DEMUtils**: rename some variables named layer in entity
- **LayeredMaterial**: separate elevation and color texture get/set methods
- **Instance**: rename removeObject() -> remove()
- **examples**: remove references to loading screen
- **examples**: use clearColor option in Instance constructor
- **examples**: use Instance.registerCRS()
- **CustomTiledImageProvider**: remove magic number
- rename Static\* to CustomTiledImage
- **StaticSource**: move code from static provider to source
- **LayeredMaterial**: remove dead code
- **LayeredMaterial**: unroll the loop in the shader itself
- **examples**: use tree shaked module imports and separate JS files
- **examples**: remove json defined layers
- **examples**: move dat.gui in package devDependencies
- **tileMesh**: move findNeighbours in TileMesh class
- **map**: move updateMinMaxDistance in Map class
- **map**: move testTileSSE in Map class
- tree-shake Three.js imports
- remove ColorTextureProcessing.js and ElevationTextureProcessing.js
- create CogSource to replace 'cog' protocol when creating layer
- delete useless instance parameter
- adapt the C3DEngine class to ES6
- remove Raster, WFS and WMS protocols
- create ColorLayer and ElevationLayer
- **examples**: use export function to access CLI options

## v0.3.0 (2022-07-04)

### BREAKING

* Instance, Map: capitalize file names: you might need to change your imports if you reference
individual files


### Features

* Add helpers method to integrate any THREE controls in giro3d
* add min/max height options on FirstPersonControls

### Fixes

* Fix picking with radius on regular THREE.js objects

### Documentation

More classes are now documented. More are even in progress!

The README has been rewritten (fix broken links, add logo, improve readability)

### others

* vscode: add tasks.json

## v0.2.0 (2022-06-16)

* Example: change the background color of orthographic.html
* Update three js to v0.135
* Fix: remove useless log
* Upgrade OpenLayers to the latest version to use the GeoTIFF loader

## v0.1.1 (2022-05-25)

* Fix: display of heightfield elevation
* Fix: fix picking on tile with heightfield elevation
* Fix: correct typo in instance.threeObjects
* Fix: also pick from THREE.Object3D we know about
* Chore: fix the repo url in package.json
* Fix: babel invocation in observer.py


## v0.1.0 (2022-05-20)

Initial release
