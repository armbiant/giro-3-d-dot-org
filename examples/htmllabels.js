import { MathUtils as THREEMath, Vector2, Vector3 } from 'three';
import TileWMS from 'ol/source/TileWMS.js';
import Vector from 'ol/source/Vector.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import Extent from '@giro3d/giro3d/core/geographic/Extent.js';
import Instance from '@giro3d/giro3d/core/Instance.js';
import ColorLayer from '@giro3d/giro3d/core/layer/ColorLayer.js';
import Map from '@giro3d/giro3d/entities/Map.js';
import Coordinates from '@giro3d/giro3d/core/geographic/Coordinates.js';
import { STRATEGY_DICHOTOMY } from '@giro3d/giro3d/core/layer/LayerUpdateStrategy.js';
import Inspector from '@giro3d/giro3d/gui/Inspector.js';
import StatusBar from './widgets/StatusBar.js';

// This example is based on planar_vector example, adding labels on features.
// You can directly jump to `geoJsonLayer.source.addEventListener('featuresloadend', ...)`,
// as the rest is similar.

Instance.registerCRS('EPSG:3946',
    '+proj=lcc +lat_1=45.25 +lat_2=46.75 +lat_0=46 +lon_0=3 +x_0=1700000 +y_0=5200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');

const extent = new Extent(
    'EPSG:3946',
    1837816.94334, 1847692.32501,
    5170036.4587, 5178412.82698,
);

const viewerDiv = document.getElementById('viewerDiv');

const instance = new Instance(viewerDiv);

const map = new Map('planar', { extent });
instance.add(map);

// Function to look at an extent from top
function lookTopDownAt(lookAtExtent, lookAtAltitude = 0) {
    const hFov = THREEMath.degToRad(instance.camera.camera3D.fov) / 2;

    const altitude = (
        Math.max(
            lookAtExtent.dimensions().x / instance.camera.camera3D.aspect,
            lookAtExtent.dimensions().y,
        ) / Math.tan(hFov)) * 0.5;
    const position = lookAtExtent.center().xyz().add(new Vector3(0, 0, altitude));
    const lookAt = lookAtExtent.center().xyz();
    lookAt.z = lookAtAltitude;
    // place camera above
    instance.camera.camera3D.position.copy(position);
    // look down
    instance.camera.camera3D.lookAt(lookAt);
    // make sure the camera isn't rotating around its view axis
    instance.camera.camera3D.rotation.z = 0;
    instance.camera.camera3D.rotation.x = 0.01; // quickfix to avoid bizarre jumps

    instance.controls.target.copy(lookAt);
    instance.controls.saveState();
    instance.notifyChange(instance.camera.camera3D);
}

const wmsSource = new TileWMS({
    url: 'https://download.data.grandlyon.com/wms/grandlyon',
    projection: 'EPSG:3946',
    crossOrigin: 'anonymous',
    params: {
        LAYERS: ['Ortho2018_Dalle_unique_8cm_CC46'],
        FORMAT: 'image/jpeg',
    },
    version: '1.3.0',
});

const colorLayer = new ColorLayer(
    'wms_imagery',
    {
        source: wmsSource,
        updateStrategy: {
            type: STRATEGY_DICHOTOMY,
            options: {},
        },
    },
);
map.addLayer(colorLayer);

const geoJsonSource = new Vector({
    url: 'https://raw.githubusercontent.com/iTowns/iTowns2-sample-data/master/lyon.geojson',
    format: new GeoJSON({ dataProjection: 'EPSG:3946' }),
});
geoJsonSource.loadFeatures();

const geoJsonLayer = new ColorLayer(
    'geo',
    {
        source: geoJsonSource,
        projection: 'EPSG:3946',
    },
);
geoJsonLayer.style = (Style, Fill, Stroke) => () => new Style({
    fill: new Fill({
        color: 'rgba(255, 165, 0, 0.2)',
        opacity: 0.2,
    }),
    stroke: new Stroke({
        color: 'white',
    }),
});
geoJsonLayer.source.addEventListener('featuresloadend', e => {
    // Traverse the OpenLayers features that were added
    for (const feature of e.features) {
        // Create a label for each feature

        const text = document.createElement('div');
        // Virtually any inner markup is supported, here we're just inserting text
        text.innerText = feature.get('nom');
        text.title = `${feature.get('numero_arrondissement')}e arrondissement`;

        // Any CSS style is supported
        text.style.color = '#ffffff';
        text.style.padding = '0.2em 1em';
        text.style.maxWidth = '200px';
        text.style.border = '2px solid #cccccc';
        text.style.backgroundColor = '#080808';
        text.style.textAlign = 'center';
        text.style.opacity = 0.8;

        // Adding the label requires a Vector3 position, let's compute that
        // We'll position the label at the center of the geometry extent
        const olExtent = feature.getGeometry().getExtent();
        const giro3dExtent = new Extent('EPSG:3946', olExtent[0], olExtent[2], olExtent[1], olExtent[3]);
        if (!giro3dExtent.isInside(extent)) {
            // The extent of the feature is not fully inside the map extent,
            // let's crop it to make sure the label will be inside the map
            giro3dExtent.intersect(extent);
        }
        const position = new Vector2();
        giro3dExtent.center(position);

        // Create our label and position it
        const label = new CSS2DObject(text);
        label.position.set(position.x, position.y, 0);
        label.updateMatrixWorld();
        // Give it a name so it shows up nicely in the inspector
        label.name = `${feature.get('nom')}`;
        // Simply add it to our instance
        // (we could also create a dedicated THREE.Group to have all the labels inside)
        instance.add(label);

        // By default, labels don't have mouse interaction enabled (pointerEvents = 'none')
        // Let's change that so we can click on it to zoom on it
        text.style.cursor = 'pointer';
        text.style.pointerEvents = 'auto';
        // Controls can interfer with the click event
        // e.g. this click event is triggered when we drag the map and the dragging ends on a label
        // but the mouseover is not, so use that to know if the user really wants to click
        // on the label.
        text.addEventListener('mouseover', () => {
            text._over = true;
        });
        text.addEventListener('mouseout', () => {
            text._over = false;
        });
        text.addEventListener('click', () => {
            if (text._over) lookTopDownAt(giro3dExtent);
        });
    }
    instance.notifyChange(geoJsonLayer);
});

map.addLayer(geoJsonLayer);

const cameraPosition = new Coordinates(
    'EPSG:3946',
    extent.west(), extent.south(), 2000,
).xyz();
instance.camera.camera3D.position.copy(cameraPosition);

const controls = new MapControls(
    instance.camera.camera3D,
    viewerDiv,
);
controls.target = extent.center().xyz();
controls.saveState();

controls.enableDamping = true;
controls.dampingFactor = 0.2;
controls.maxPolarAngle = Math.PI / 2.3;

instance.useTHREEControls(controls);

Inspector.attach(document.getElementById('panelDiv'), instance);
StatusBar.bind(instance);
